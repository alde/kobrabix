$:.push File.expand_path("../lib", __FILE__)

require "rake"

Gem::Specification.new do |s|
    s.platform      = Gem::Platform::RUBY
    s.authors       = ['Rickard']
    s.email         = ['rickard.dybeck@klarna.com']
    s.homepage      = "http://integration.klarna.com"
    s.summary       = "BIX/sendgrid email queries"
    s.description   = "A gem to manage queries to BIX"
    s.name          = "kobra_bix"
    s.version       = "0.0.5"
    s.date          = "2013-04-18"
    s.files         = FileList['lib/**/*.rb']
    s.require_paths = ["lib"]
    s.add_dependency "pg", "~> 0.15"
    s.add_dependency "diskcached", "~> 1.1"
end
