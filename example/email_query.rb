require "kobra_bix"

def time
  puts "Query start:\n-------------------------------------\n"
  start = Time.now
  yield
  puts "Query end:\n-------------------------------------\n"
  puts "Query time " + (Time.now - start).to_s + "\n\n"
end


Kobra::BixMailQuery.configure({
  dbname: '<database name>',
  host: '<database server>',
  port: '<port>',
  user: '<username>',
  password: '<password>'
})
bmq = Kobra::BixMailQuery.new
bmq.clear_cache
time { puts bmq.query "absolut_vanilj-vodka__@live.se" }
time { puts bmq.query "absolut_vanilj-vodka__@live.se" }

time { puts bmq.query "foo@bar.se" }
