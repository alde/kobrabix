module Kobra
  class SendGrid

    require "net/http"

    protected
    attr_accessor :http, :api_user, :api_key

    public
    def initialize api_user, api_key
      @api_user, @api_key = api_user, api_key
      uri = URI.parse("https://sendgrid.com")

      @http = Net::HTTP.new(uri.host, uri.port)

      @http.use_ssl = true

    end

    def unblock email
      resource = "/api/blocks.delete.json?" +
        "api_user=#{@api_user}&" +
        "api_key=#{@api_key}&" +
        "email=#{email}"

      @http.delete(resource).body
    end

    def get_blocks
      resource = "/api/blocks.get.json?" +
        "api_user=#{@api_user}&" +
        "api_key=#{@api_key}&" +
        "date=1"

      @http.get(resource).body
    end
  end
end
