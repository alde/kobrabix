require "pg"
require "diskcached"

module Kobra
  class BixMailQuery

    protected
    attr_accessor :db, :cache

    @@data = {}

    public
    def initialize
      @db = PG::Connection.new(
        dbname: @@data[:dbname],
        host: @@data[:host],
        port: @@data[:port],
        user: @@data[:user],
        password: @@data[:password],
        sslmode: :require
      )
      @cache = Diskcached.new
      clear_cache
    end

    ##
    # configure BixMailQuery with postgres data
    # {
    #    dbname: '<database name>',
    #    host: '<database server>',
    #    port: '<port>',
    #    user: '<username>',
    #    password: '<password>'
    # }
    def self.configure hash
      @@data = hash
    end

    def query email
      begin
        result = @cache.get(email)
      rescue Diskcached::NotFound
        result = actual_query email
        @cache.set(email, result)
      end
      {data: result}
    end

    def clear_cache
      @cache.flush
    end

    private
    def actual_query email
        data = []
        result = @db.query("SELECT id, event, email, category, response, event_timestamp_millis AS time, type, reason FROM t_sendgrid_event WHERE email = '#{email}' ORDER BY event_timestamp_millis DESC")
        result.each do |row|
          data << row
        end
        data
    end
  end
end
